#!/bin/sh
mkdir -p godot/export
mkdir -p godot/files/bin
mkdir -p .tmp
curl -sSL https://downloads.tuxfamily.org/godotengine/2.1.4/Godot_v2.1.4-stable_x11.64.zip > .tmp/godot.zip
unzip .tmp/godot.zip -d .tmp
mv .tmp/Godot_v2.1.4-stable_x11.64 godot/files/bin/godot
cp metadata godot/
rm -rf .tmp
flatpak build-export repo godot
flatpak build-bundle repo godot.flatpak nl.gbraad.Godot
